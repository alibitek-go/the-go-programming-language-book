package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

func echo(c net.Conn, shout string, delay time.Duration) {
	fmt.Fprintln(c, "\t", strings.ToUpper(shout))
	time.Sleep(delay)

	fmt.Fprintln(c, "\t", shout)
	time.Sleep(delay)

	fmt.Fprintln(c, "\t", strings.ToLower(shout))
}

func handleConn(c net.Conn) {
	defer c.Close()

	inputChan := make(chan string)
	go func() {
		input := bufio.NewScanner(c)
		for input.Scan() {
			inputChan <- input.Text()
		}
	}()

LOOP:
	for {
		select {
		case <-time.After(10 * time.Second):
			log.Println("Disconnecting client due to not shouting anything in the last 10 seconds")
			break LOOP
		case inputText := <-inputChan:
			go func(text string) {
				echo(c, text, 1*time.Second)
			}(inputText)
		}
	}
}

func main() {
	var port = flag.Int("port", 8000, "the port to run the server under")
	flag.Parse()

	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err) // e.g. connection aborted
			continue
		}

		go handleConn(conn) // handle connections concurrently
	}
}
