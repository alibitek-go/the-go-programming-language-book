package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/alibitek-go/the-go-programming-language-book/chapter8-goroutines-and-channels/web-crawler/links"
)

/*
	Counting semaphore - implemented using a buffered channel of capacity n

	Conceptually, each of the n vacant slots in the channel buffer represents a token entitling the holder to proceed.

	Sending a value into the channel aquires a token
	Receiving a value from the channel releases a token, creating a new vacant slot.

	This ensures that at most n sends can occur without an intervening receive.

	(Although it might be more intuitive to treat filled slots in the channel buffer as tokens,
	using vacant slots avoids the need to fill the channel buffer after creating it.)

	Since the channel element type is not important, we'll use struct{}, which has size zero.
*/

// tokens is a counting semaphore used to enforce a limit of 20 concurrent requests
var tokens = make(chan struct{}, 20)

func crawl(url string) []string {
	fmt.Println("Crawling", url)

	// aquire a token
	tokens <- struct{}{}

	list, err := links.Extract(url)

	// release a token
	<-tokens

	if err != nil {
		log.Print(err)
	}
	return list
}

func main1() {
	// queue of items that need processing
	// each item is a list of URLs to crawl
	workList := make(chan []string)
	var pendingSends int // number of pending sends to worklist

	// Start with command-line arguments
	pendingSends++
	go func() {
		log.Println(os.Args[1:])
		workList <- os.Args[1:]
	}()

	// Crawl the web concurrently
	seen := make(map[string]bool)

	for ; pendingSends > 0; pendingSends-- {
		list := <-workList
		for _, link := range list {
			if !seen[link] {
				seen[link] = true
				pendingSends++
				go func(link string) {
					workList <- crawl(link)
				}(link)
			}
		}
	}
}

func crawl2(url string) []string {
	list, err := links.Extract(url)

	if err != nil {
		log.Print(err)
	}
	return list
}

type workData struct {
	links []string
	depth int
}

type unseenWorkData struct {
	link  string
	depth int
}

func main2() {
	var maxDepth int
	flag.IntVar(&maxDepth, "depth", 1, "-depth <depth level>")
	flag.Parse()

	// list of URLs, may have duplicates
	worklist := make(chan workData)
	unseenLinks := make(chan unseenWorkData) // de-duplicated URLs
	var numberOfCrawledURLs uint32
	var wg sync.WaitGroup

	// Add command-line arguments to worklist
	go func() {
		worklist <- workData{links: os.Args[1:], depth: 0}
	}()

	// Create 20 crawler goroutines to fetch each unseen link
	for i := 0; i < 5; i++ {
		go func(id int) {
			defer log.Println("Destroying go routine", id)
			defer wg.Done()

			wg.Add(1)

			for unseenWorkData := range unseenLinks {
				log.Printf("[Go routine %d] Crawling url: %s\n", id, unseenWorkData.link)
				atomic.AddUint32(&numberOfCrawledURLs, 1)
				foundLinks := crawl2(unseenWorkData.link)

				go func(depth int) { worklist <- workData{links: foundLinks, depth: depth} }(unseenWorkData.depth + 1)
			}
		}(i)
	}

	// The main goroutine de-duplicates worklist items
	// and sendsthe unseen ones to the crawlers
	seen := make(map[string]bool)

LOOP:
	for {
		select {
		case workData := <-worklist:
			for _, link := range workData.links {
				if !seen[link] && workData.depth <= maxDepth {
					seen[link] = true
					unseenLinks <- unseenWorkData{link: link, depth: workData.depth}
				}
			}
		case <-time.After(2 * time.Second):
			close(unseenLinks)
			break LOOP
		}
	}

	wg.Wait()
	log.Println("Main done", "Crawled URLs:", atomic.LoadUint32(&numberOfCrawledURLs))
}

func main() {
	// main1()
	main2()
}
