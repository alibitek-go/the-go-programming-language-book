package main

import (
	"fmt"
	"os"
	"time"
)

func launch() {
	fmt.Println("Rocket launch!!!")
}

func main1() {
	abort := make(chan struct{})
	go func() {
		os.Stdin.Read(make([]byte, 1)) // read a single byte
		abort <- struct{}{}
	}()

	fmt.Println("Commencing countdown. Press return to abort.")
	ticker := time.NewTicker(1 * time.Second)

	for countdown := 10; countdown > 0; countdown-- {
		fmt.Println(countdown)

		select {
		case <-ticker.C:
		case <-abort:
			fmt.Println("Launch aborted!")
			ticker.Stop()
			return
		}
	}

	launch()

	// wait forever
	select {}
}

func main2() {
	fmt.Println("Commencing countdown. Press return to abort")

	abort := make(chan struct{})
	go func() {
		os.Stdin.Read(make([]byte, 1))
		abort <- struct{}{}
	}()

	select {
	case <-time.After(10 * time.Second):
	case <-abort:
		fmt.Println("Launch aborted!")
		return
	}
	launch()
}

func main3() {
	ch := make(chan int, 1)

	for i := 0; i < 10; i++ {
		select {
		case x := <-ch: // i is odd: 1, 3, 5, 7, 9
			fmt.Println("[Receive] i=", i, "x=", x)
		case ch <- i: // is is even: 0, 2, 4, 6, 8
			fmt.Println("[Send] i=", i)
		}
	}
}

func main4() {
	abort := make(chan struct{})

	time.AfterFunc(100*time.Millisecond, func() {
		abort <- struct{}{}
	})

	for {
		// channel polling
		select {
		case <-abort:
			fmt.Printf("Launch aborted\n")
			return
		default:
			// do nothing
			fmt.Println("Waiting...")
		}
	}
}

func main5() {
	// nil channels are never selected in a select statement
	// This lets us use nil to enable or disable cases that corespond to features like handling timeouts or cancellation,
	// responding to other input events, or emitting output
	var nilChan chan struct{}

	go func() {
		select {
		case <-nilChan:
			fmt.Println("nil channel receive")
		default:
			fmt.Println("Receive on nil channel blocks forever")
		}
	}()

	select {
	case nilChan <- struct{}{}:
		fmt.Println("nil channel send")
	default:
		fmt.Println("Send on nil channel blocks forever")
	}

	time.Sleep(1 * time.Second)
}

func main() {
	main5()
}
