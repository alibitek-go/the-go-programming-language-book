package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

type clockServer struct {
	region   string
	hostPort string
}

var wg sync.WaitGroup

func (cs clockServer) String() string {
	return fmt.Sprintf("clockServer{Region: %s, HostPort: %s}", cs.region, cs.hostPort)
}

func main() {
	clockServers := getClockServers()

	for _, clockServer := range clockServers {
		wg.Add(1)
		go connectToClockServer(clockServer)
	}

	wg.Wait()
}

func getClockServers() []clockServer {
	clockServersArgs := os.Args[1:]
	if len(clockServersArgs) == 0 {
		log.Fatal("Please provide at least one clock server as argument, e.g. NewYork=localhost:8010")
	}

	clockServers := make([]clockServer, 0, len(clockServersArgs))
	for i, clockServerArg := range clockServersArgs {
		log.Println("Parsing", clockServerArg)
		parts := strings.Split(clockServerArg, "=")
		if len(parts) == 0 {
			log.Println("Skipping invalid clock server argument at index", i)
			continue
		}
		clockServers = append(clockServers, clockServer{
			parts[0],
			parts[1],
		})
	}

	return clockServers
}

func connectToClockServer(server clockServer) {
	log.Printf("Connecting to clock server: %s\n", server)
	conn, err := net.Dial("tcp", server.hostPort)
	if err != nil {
		log.Fatalf("Cannot connect to clock server: %s, %s, reason: %s", server.region, server.hostPort, err)
	}
	defer conn.Close()

	mustCopy(server, os.Stdout, conn)
}

func mustCopy(server clockServer, dst io.Writer, src io.Reader) {
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}
