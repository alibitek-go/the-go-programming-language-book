package main

import "fmt"

func counter(naturals chan<- int) {
	defer close(naturals)

	for x := 0; x < 100; x++ {
		naturals <- x
	}
}

func squarer(naturals <-chan int, squares chan<- int) {
	defer close(squares)

	for {
		x, ok := <-naturals
		if !ok { // naturals channel was closed and drained
			break
		}
		squares <- x * x
	}
}

func printer(squares <-chan int) {
	for x := range squares {
		fmt.Println(x)
	}
}

func main() {
	naturals := make(chan int)
	squares := make(chan int)

	// Counter
	go counter(naturals)

	// Squarer
	go squarer(naturals, squares)

	// Printer
	printer(squares)
}
